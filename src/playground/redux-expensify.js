import { createStore, combineReducers } from "redux";
import uuid from "uuid";

// const demoState = {
//   expenses: [
//     {
//       id: "werty-09876543",
//       description: "January Rent",
//       note: "This was the final payment for that address",
//       amount: 54500,
//       createdAt: 0
//     }
//   ],
//   filters: {
//     text: "rent",
//     sortBy: "date|amount",
//     startDate: undefined,
//     endDate: undefined
//   }
// };

// Expenses Reducer
const addExpense = ({
  description = "",
  note = "",
  amount = 0,
  createdAt = 0
} = {}) => ({
  type: "ADD_EXPENSE",
  expense: {
    id: uuid(),
    description,
    note,
    amount,
    createdAt
  }
});

const removeExpense = ({ id = "" } = {}) => ({
  type: "REMOVE_EXPENSE",
  id
});

const editExpense = (id, updates) => ({
  type: "EDIT_EXPENSE",
  expense: { id, updates }
});

const expensesDefaultState = [];
const expensesReducer = (state = expensesDefaultState, action) => {
  switch (action.type) {
    case "ADD_EXPENSE":
      return [...state, action.expense];

    case "REMOVE_EXPENSE":
      return state.filter(({ id }) => id !== action.id);

    case "EDIT_EXPENSE":
      return state.map(expense => {
        if (expense.id === action.expense.id) {
          return { ...expense, ...action.expense.updates };
        }
        return expense;
      });

    default:
      return state;
  }
};

// Filters Reducer
const setTextFilter = (text = "") => ({
  type: "SET_TEXT_FILTER",
  text
});

const sortByAmount = () => ({ type: "SORT_BY_AMOUNT" });
const sortByDate = () => ({ type: "SORT_BY_DATE" });
const setStartDate = (timestamp = undefined) => ({
  type: "SET_START_DATE",
  timestamp
});
const setEndDate = (timestamp = undefined) => ({
  type: "SET_END_DATE",
  timestamp
});

const filtersDefaultState = {
  text: "",
  sortBy: "date",
  startDate: undefined,
  endDate: undefined
};
const filtersReducer = (state = filtersDefaultState, action) => {
  switch (action.type) {
    case "SET_TEXT_FILTER":
      return { ...state, text: action.text };
    case "SORT_BY_AMOUNT":
      return { ...state, sortBy: "amount" };
    case "SORT_BY_DATE":
      return { ...state, sortBy: "date" };
    case "SET_START_DATE":
      return { ...state, startDate: action.timestamp };
    case "SET_END_DATE":
      return { ...state, endDate: action.timestamp };
    default:
      return state;
  }
};

const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) => {
  return expenses
    .filter(expense => {
      // If startDate is not a number then it's a match (because there's no way of do the match),
      // otherwise try to match with the expression.
      const startDateMatch =
        typeof startDate !== "number" || expense.createdAt >= startDate;
      const endDateMatch =
        typeof endDate !== "number" || expense.createdAt <= endDate;
      const textMatch =
        typeof text !== "string" ||
        expense.description.toLowerCase().includes(text.toLowerCase());

      return startDateMatch && endDateMatch && textMatch;
    })
    .sort((currentExpense, nextExpense) => {
      if (sortBy === "date") {
        return currentExpense.createdAt > nextExpense.createdAt ? 1 : -1;
      }

      if (sortBy === "amount") {
        return currentExpense.amount > nextExpense.amount ? 1 : -1;
      }

      return 0;
    });
};

const store = createStore(
  combineReducers({
    expenses: expensesReducer,
    filters: filtersReducer
  })
);

store.subscribe(() => {
  const state = store.getState();
  const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);
  console.log(visibleExpenses);
});

const expenseOne = store.dispatch(
  addExpense({ description: "Rent", amount: 355000, createdAt: 125 })
);
const expenseTwo = store.dispatch(
  addExpense({ description: "Coffee", amount: 5500, createdAt: 500 })
);

// store.dispatch(removeExpense({ id: expenseOne.expense.id }));
// store.dispatch(editExpense(expenseTwo.expense.id, { amount: 6500 }));

// store.dispatch(setTextFilter("rent"));
// store.dispatch(setStartDate(100));
// store.dispatch(setEndDate(510));

// store.dispatch(sortByAmount());
store.dispatch(sortByDate());
// store.dispatch(setStartDate());

// console.log(expenseOne);

import * as firebase from 'firebase';

const firebaseConfig = {
  apiKey: 'AIzaSyCvX5QVzGJ6Nupnujg0QQcNyaC5pRs3Qc8',
  authDomain: 'expensify-6811d.firebaseapp.com',
  databaseURL: 'https://expensify-6811d.firebaseio.com',
  projectId: 'expensify-6811d',
  storageBucket: 'expensify-6811d.appspot.com',
  messagingSenderId: '941314937939',
  appId: '1:941314937939:web:dfa8c1a87937bc8545a2a6',
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const database = firebase.database();

// child_changed
database.ref('expenses').on('child_changed', (childSnapshot, prevChildKey) => {
  console.log(childSnapshot.key, prevChildKey, childSnapshot.val());
});

// child_removed
database.ref('expenses').on('child_removed', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

// child_added. Called once for all the existing data
// and when new childs are added to the reference
database.ref('expenses').on('child_added', (snapshot) => {
  console.log(snapshot.key, snapshot.val());
});

// const onDataChanged = database.ref('expenses').on(
//   'value',
//   (snapshot) => {
//     const expenses = [];

//     snapshot.forEach((childSnapshot) => {
//       expenses.push({ id: childSnapshot.key, ...childSnapshot.val() });
//     });

//     console.table(expenses);
//   },
//   (e) => console.error('Failed to fetch data: ', e.message)
// );

// database
//   .ref('expenses')
//   .once('value')
//   .then((snapshot) => {
//     const expenses = [];

//     snapshot.forEach((childSnapshot) => {
//       expenses.push({ id: childSnapshot.key, ...childSnapshot.val() });
//     });

//     console.log(expenses);
//   });

// database
//   .ref('expenses')
//   .push({ description: 'Rent', amount: 950000, createdAt: 0 });
// database
//   .ref('expenses')
//   .push({ description: 'Internet Bill', amount: 50050, createdAt: 0 });
// database
//   .ref('expenses')
//   .push({ description: 'Coffee', amount: 8050, createdAt: 0 });

// database.ref('notes/-M19Rn84wrkEoadrW195').update({ body: 'Buy Food' });
// database.ref('notes/-M19Rn84wrkEoadrW195').remove();

// const notes = [
//   { id: '12', title: 'first note', body: 'This is my note' },
//   { id: '765ase', title: 'second note', body: 'This is my second note' },
// ];

// database
//   .ref('notes')
//   .set(notes)
//   .catch((e) => console.log(e));

// ref() gets a reference from the database
// if not specified gets a reference to the root of the database

// Creates a new data subscription...
// const onValueChanged = database.ref().on(
//   'value',
//   (snapshot) => console.log(snapshot.val()),
//   (e) => console.log('Error with data fetching: ', e.message)
// );

// Turning off the reference subscription...
// setTimeout(() => {
//   database.ref().off(onValueChanged);
// }, 5000);

// const onDataChanged = database.ref().on(
//   'value',
//   (snapshot) => {
//     const { name, job: { title: jobTitle, company } = {} } = snapshot.val();

//     console.log(`${name} is a ${jobTitle} at ${company}`);
//   },
//   (e) => console.log('An error ocurred while fetching the data: ', e.message)
// );

// database
//   .ref()
//   .once('value')
//   .then((snapshot) => console.log(snapshot.val()))
//   .catch((error) => console.error(error.message));

// database
//   .ref()
//   .set({
//     name: 'Uziel Bueno',
//     age: 34,
//     isSingle: true,
//     stressLevel: 6,
//     job: {
//       title: 'Software Developer',
//       company: 'Google',
//     },
//     location: {
//       city: 'Cuernavaca',
//       country: 'Mexico',
//     },
//   })
//   .then(() => {
//     console.log('Data is saved!');
//   })
//   .catch((error) => {
//     console.log('This failed: ', error);
//   });

// Remove a reference from the database...
// database
//   .ref('isSingle')
//   .remove()
//   .then(() => console.log('isSingle was removed!'))
//   .catch((error) =>
//     console.log('Could not remove isSingle from the database: ', error)
//   );

// Removing data with set using a null value...
// database.ref('isSingle').set(null);

// database.ref().update({
//   stressLevel: 9,
//   'job/company': 'Amazon',
//   'location/city': 'Seattle',
//   'location/country': 'United States',
// });

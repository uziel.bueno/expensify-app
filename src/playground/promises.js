const promise = new Promise((resolve, reject) => {
  // Long running task like database interaction...
  setTimeout(() => {
    return resolve('This is my resolved data');
    // return reject('Whooops! An error occurred.');
  }, 5000);
});

(async () => {
  try {
    const data = await promise;
    console.log(data);
  } catch (error) {
    console.log(error);
  }
})();

// promise.then((data) => console.log(data)).catch((error) => console.log(error));

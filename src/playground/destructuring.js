// Object Destructuring

// const person = {
//   name: "Uziel",
//   age: 34,
//   location: {
//     city: "Cuernavaca",
//     temp: 24
//   }
// };

// const { name: firstName = "Anonymous", age } = person;

// console.log(`${firstName} is ${age}.`);

// const { city, temp: temperature } = person.location;

// if (temperature && city) {
//   console.log(`It's ${temperature}  in ${city}`);
// }

const book = {
  title: "Ego is the Enemy",
  author: "Ryan Holiday",
  publisher: {
    name: "Penguin"
  }
};

const { name: publisherName = "Self-Published" } = book.publisher;

console.log(publisherName);

// Array Destructuring

const address = [
  "1299 S Juniper Street",
  "Philadelphia",
  "Pennsylvania",
  "19147"
];

const [street, city = "Unknown", state = "Unknown", zip] = address;

console.log(`You are in ${city} ${state}.`);

const item = ["Coffee (hot)", "$2.00", "$2.50", "$2.75"];

const [product, , mediumPrice] = item;

console.log(`A medium ${product} costs ${mediumPrice}`);

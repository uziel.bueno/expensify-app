import React from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import HelpPage from '@components/HelpPage';
import PrivateRoute from './PrivateRoute';
import LoginPage from '@/components/LoginPage';
import NotFoundPage from '@components/NotFoundPage';
import AddExpensePage from '@components/AddExpensePage';
import EditExpensePage from '@components/EditExpensePage';
import ExpenseDashboardPage from '@components/ExpenseDashboardPage';
import PublicRoute from './PublicRoute';

export const history = createBrowserHistory();

const AppRouter = (props) => (
  <Router history={history}>
    <div>
      <Switch>
        <PublicRoute path="/" component={LoginPage} exact={true} />
        <PrivateRoute path="/dashboard" component={ExpenseDashboardPage} />
        <PrivateRoute path="/create" component={AddExpensePage} />
        <PrivateRoute path="/edit/:id" component={EditExpensePage} />
        <Route path="/help" component={HelpPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </Router>
);

export default AppRouter;

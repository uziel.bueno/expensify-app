import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import expensesReducer from '@/reducers/expenses';
import filtersReducer from '@/reducers/filters';
import authReducer from '@/reducers/auth';

// Reducers...
const reducer = combineReducers({
  auth: authReducer,
  expenses: expensesReducer,
  filters: filtersReducer,
});

// Enhancers...
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const enhancer = composeEnhancers(applyMiddleware(thunk));

export default () => {
  return createStore(reducer, enhancer);
};

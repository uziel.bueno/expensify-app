import moment from 'moment';

export default [
  { id: '1', description: 'Rent', amount: 950000, createdAt: 0, note: '' },
  {
    id: '2',
    description: 'Coffee',
    amount: 6550,
    createdAt: moment(0)
      .subtract(5, 'days')
      .valueOf(),
    note: '',
  },
  {
    id: '3',
    description: 'Credit Card',
    amount: 350000,
    createdAt: moment(0)
      .add(5, 'days')
      .valueOf(),
    note: '',
  },
];

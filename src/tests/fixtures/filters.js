import moment from 'moment';

export const filters = {
  text: '',
  sortBy: 'date',
  startDate: undefined,
  endDate: undefined,
};

export const altFilters = {
  text: 'bills',
  sortBy: 'date',
  startDate: moment('2020-02-23'),
  endDate: moment('2020-02-25'),
};

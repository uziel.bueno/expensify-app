import moment from 'moment';
import getVisibleExpenses from '../../selectors/expenses';
import expenses from '../fixtures/expenses';

test('should filter by text value', () => {
  const filters = {
    text: 'e',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined,
  };

  const filteredExpenses = getVisibleExpenses(expenses, filters);

  expect(filteredExpenses).toEqual([expenses[2], expenses[0], expenses[1]]);
});

test('should filter expenses by startDate', () => {
  const filters = {
    text: '',
    sortBy: 'date',
    startDate: moment(0),
    endDate: undefined,
  };

  const result = getVisibleExpenses(expenses, filters);
  expect(result).toEqual([expenses[2], expenses[0]]);
});

test('should filter expenses by endDate', () => {
  const filters = {
    startDate: undefined,
    endDate: moment(0),
  };

  const result = getVisibleExpenses(expenses, filters);

  expect(result).toEqual([expenses[0], expenses[1]]);
});

test('should sort expenses by date in descending order', () => {
  const filters = {
    text: '',
    sortBy: 'date',
    startDate: undefined,
    endDate: undefined,
  };

  const result = getVisibleExpenses(expenses, filters);

  expect(result).toEqual([expenses[2], expenses[0], expenses[1]]);
});

test('should sort expenses by amount in descending order', () => {
  const filters = {
    sortBy: 'amount',
  };

  const result = getVisibleExpenses(expenses, filters);

  expect(result).toEqual([expenses[0], expenses[2], expenses[1]]);
});

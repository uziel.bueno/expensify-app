import selectExpensesTotal from '@/selectors/expenses-total';
import moment from 'moment';

let expenses = [];

beforeEach(() => {
  expenses = [
    {
      description: 'Rent',
      amount: 1000000,
      createdAt: moment('2020-02-26').valueOf(),
      note: 'February 2020 Rent bill',
    },
    {
      description: 'Special Coffee',
      amount: 10000,
      createdAt: moment('2020-02-26').valueOf(),
      note: 'February 2020 Rent bill',
    },
    {
      description: 'Internet Bill',
      amount: 50000,
      createdAt: moment('2020-02-26').valueOf(),
      note: 'February 2020 Internet bill',
    },
  ];
});

test('should return 0 if there are no expenses', () => {
  const total = selectExpensesTotal(undefined);

  expect(total).toBe(0);
});

test('should correctly add up a single expense', () => {
  const total = selectExpensesTotal([expenses[0]]);

  expect(total).toBe(1000000);
});

test('should correctly add up multiple expenses', () => {
  const total = selectExpensesTotal(expenses);

  expect(total).toBe(1060000);
});

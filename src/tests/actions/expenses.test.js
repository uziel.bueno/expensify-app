import moment from 'moment';
import thunk from 'redux-thunk';
import database from '@/firebase/firebase';
import configureMockStore from 'redux-mock-store';
import {
  addExpense,
  startAddExpense,
  editExpense,
  startEditExpense,
  removeExpense,
  startRemoveExpense,
  setExpenses,
  startSetExpenses,
} from '../../actions/expenses';
import expenses from '../fixtures/expenses';

const uid = 'thisismytestuid';
const defaultAuthState = { auth: { uid } };
const createMockStore = configureMockStore([thunk]);

beforeEach((done) => {
  const expenseData = {};

  expenses.forEach(({ id, description, amount, createdAt, note }) => {
    expenseData[id] = { id, description, amount, createdAt, note };
  });

  database
    .ref(`users/${uid}/expenses`)
    .set(expenseData)
    .then(() => done());
});

test('should setup remove expense action object', () => {
  const action = removeExpense({ id: 'the-expense-uuid' });
  expect(action).toEqual({
    type: 'REMOVE_EXPENSE',
    id: 'the-expense-uuid',
  });
});

test('should remove an expense stored in firebase', (done) => {
  const store = createMockStore(defaultAuthState);
  const expenseId = expenses[0].id;

  store
    .dispatch(startRemoveExpense({ id: expenseId }))
    .then(() => {
      const actions = store.getActions();

      expect(actions[0]).toEqual({
        type: 'REMOVE_EXPENSE',
        id: expenseId,
      });

      return database.ref(`users/${uid}/expenses/${expenseId}`).once('value');
    })
    .then((snapshot) => {
      expect(snapshot.val()).toBeNull();
      done();
    });
});

test('should setup edit expense action object', () => {
  const action = editExpense('the-expense-id', {
    description: 'New description',
    amount: 4500,
    createdAt: 4000,
    note: 'Fake note',
  });

  expect(action).toEqual({
    type: 'EDIT_EXPENSE',
    expense: {
      id: 'the-expense-id',
      updates: {
        description: 'New description',
        amount: 4500,
        createdAt: 4000,
        note: 'Fake note',
      },
    },
  });
});

test('should edit an expense stored in firebase', (done) => {
  const store = createMockStore(defaultAuthState);
  const expenseId = expenses[0].id;
  const updates = {
    description: 'Updated expense',
    amount: 4500,
    createdAt: moment('2020-03-04').valueOf(),
    note: 'This is a modified expense.',
  };

  store
    .dispatch(startEditExpense(expenseId, updates))
    .then(() => {
      const actions = store.getActions();

      expect(actions[0]).toEqual({
        type: 'EDIT_EXPENSE',
        expense: { id: expenseId, updates },
      });

      return database.ref(`users/${uid}/expenses/${expenseId}`).once('value');
    })
    .then((snapshot) => {
      expect(snapshot.val().description).toBe(updates.description);
      expect(snapshot.val().amount).toBe(updates.amount);
      expect(snapshot.val().createdAt).toBe(updates.createdAt);
      expect(snapshot.val().note).toBe(updates.note);
      done();
    });
});

test('should setup add expense action object with provided values', () => {
  const action = addExpense(expenses[2]);

  expect(action).toEqual({
    type: 'ADD_EXPENSE',
    expense: expenses[2],
  });
});

test('should add expense to database and store', (done) => {
  const store = createMockStore(defaultAuthState);
  const expenseData = {
    description: 'Mouse',
    amount: 3000,
    createdAt: 1000,
    note: 'Gaming Mouse',
  };

  store
    .dispatch(startAddExpense(expenseData))
    .then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: 'ADD_EXPENSE',
        expense: {
          id: expect.any(String),
          ...expenseData,
        },
      });

      return database
        .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
        .once('value');
    })
    .then((snapshot) => {
      expect(snapshot.val()).toEqual(expenseData);
      done();
    });
});

test('should add expense with default values to database and store', (done) => {
  const store = createMockStore(defaultAuthState);
  const expenseDefaultData = {
    description: '',
    amount: 0,
    createdAt: 0,
    note: '',
  };

  store
    .dispatch(startAddExpense(undefined))
    .then(() => {
      const actions = store.getActions();

      expect(actions[0]).toEqual({
        type: 'ADD_EXPENSE',
        expense: {
          id: expect.any(String),
          ...expenseDefaultData,
        },
      });

      return database
        .ref(`users/${uid}/expenses/${actions[0].expense.id}`)
        .once('value');
    })
    .then((snapshot) => {
      expect(snapshot.val()).toEqual(expenseDefaultData);
      done();
    });
});

test('should setup set expenses action object with data', () => {
  const action = setExpenses(expenses);

  expect(action).toEqual({ type: 'SET_EXPENSES', expenses });
});

test('should fetch the expenses from firebase', (done) => {
  const store = createMockStore(defaultAuthState);

  store.dispatch(startSetExpenses()).then(() => {
    const actions = store.getActions();

    expect(actions[0]).toEqual({ type: 'SET_EXPENSES', expenses });
    done();
  });
});

import moment from 'moment';
import filtersReducer from '../../reducers/filters';
import {
  sortByDate,
  setTextFilter,
  sortByAmount,
  setStartDate,
  setEndDate,
} from '../../actions/filters';

test('should setup default filter values', () => {
  const state = filtersReducer(undefined, { type: '@@INIT' });

  expect(state).toEqual({
    text: '',
    sortBy: 'date',
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month'),
  });
});

test('should set sortBy to "amount"', () => {
  const currentState = { sortBy: 'date' };

  const state = filtersReducer(currentState, sortByAmount());

  expect(state.sortBy).toBe('amount');
});

test('should set sortBy to "date"', () => {
  const currentState = {
    sortBy: 'amount',
  };

  const state = filtersReducer(currentState, sortByDate());

  expect(state.sortBy).toBe('date');
});

test('should set text filter', () => {
  const currentState = {
    text: 'Coffee',
  };

  const state = filtersReducer(currentState, setTextFilter('Rent'));

  expect(state.text).toBe('Rent');
});

test('should set startDate filter', () => {
  const currentState = {
    startDate: undefined,
  };

  const state = filtersReducer(
    currentState,
    setStartDate(moment('2020-02-15'))
  );

  expect(state.startDate).toEqual(moment('2020-02-15'));
});

test('should set endDate filter', () => {
  const currentState = {
    endDate: undefined,
  };

  const state = filtersReducer(currentState, setEndDate(moment('2020-02-20')));

  expect(state.endDate).toEqual(moment('2020-02-20'));
});

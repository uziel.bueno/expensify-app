import moment from 'moment';
import expensesReducer from '../../reducers/expenses';
import expenses from '../fixtures/expenses';
import {
  addExpense,
  removeExpense,
  editExpense,
  setExpenses,
} from '../../actions/expenses';

test('should set default state', () => {
  const state = expensesReducer(undefined, { type: '@@INIT' });
  expect(state).toEqual([]);
});

test('should remove expense by id', () => {
  const state = expensesReducer(
    expenses,
    removeExpense({ id: expenses[0].id })
  );

  expect(state).toHaveLength(2);
  expect(state).toEqual([expenses[1], expenses[2]]);
});

test('should not remove expenses if id does not exists', () => {
  const state = expensesReducer(expenses, removeExpense({ id: '-1' }));

  expect(state).toHaveLength(3);
  expect(state).toEqual(expenses);
});

test('should add an expense', () => {
  const expense = {
    description: 'Internet Bill',
    amount: 45000,
    createdAt: moment('2020-02-21').valueOf(),
    note: 'February 2020',
  };

  const state = expensesReducer(expenses, addExpense(expense));

  expect(state).toHaveLength(4);
  expect(state).toEqual([...expenses, expense]);
});

test('should edit an expense', () => {
  const updates = {
    description: 'Coffee',
    amount: 7500,
    createdAt: moment('2020-02-21').valueOf(),
    note: '',
  };

  const state = expensesReducer(expenses, editExpense(expenses[0].id, updates));

  expect(state[0]).toEqual({ ...updates, id: expenses[0].id });
});

test('should not edit expense if id does not exists', () => {
  const updates = {
    description: 'Coffee',
    amount: 7500,
    createdAt: moment('2020-02-21').valueOf(),
  };

  const state = expensesReducer(expenses, editExpense('-1', updates));

  expect(state).toEqual(expenses);
});

test('should set expenses', () => {
  const state = expensesReducer(undefined, setExpenses([expenses[0]]));

  expect(state).toEqual([expenses[0]]);
});

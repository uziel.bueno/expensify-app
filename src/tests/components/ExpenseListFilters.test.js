import React from 'react';
import { shallow } from 'enzyme';
import moment from 'moment';
import { ExpenseListFilters } from '@components/ExpenseListFilters';
import { filters, altFilters } from '../fixtures/filters';

let wrapper,
  setTextFilterSpy,
  sortByDateSpy,
  sortByAmountSpy,
  setStartDateSpy,
  setEndDateSpy;

beforeEach(() => {
  setTextFilterSpy = jest.fn();
  sortByDateSpy = jest.fn();
  sortByAmountSpy = jest.fn();
  setStartDateSpy = jest.fn();
  setEndDateSpy = jest.fn();
  wrapper = shallow(
    <ExpenseListFilters
      setTextFilter={setTextFilterSpy}
      sortByDate={sortByDateSpy}
      sortByAmount={sortByAmountSpy}
      setStartDate={setStartDateSpy}
      setEndDate={setEndDateSpy}
      filters={filters}
    />
  );
});

test('should render ExpenseListFilters correctly', () => {
  expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseListFilters with alternate filter data', () => {
  wrapper.setProps({ filters: altFilters });

  expect(wrapper).toMatchSnapshot();
});

test('should handle text change', () => {
  wrapper
    .find('input[name="text"]')
    .simulate('change', { target: { value: 'coff' } });

  expect(setTextFilterSpy).toHaveBeenLastCalledWith('coff');
});

test('should handle sort by date', () => {
  wrapper.setProps({ filters: { ...altFilters, sortBy: 'amount' } });
  wrapper
    .find('select[name="sort"]')
    .simulate('change', { target: { value: 'date' } });

  expect(sortByDateSpy).toHaveBeenCalled();
});

test('should handle date changes', () => {
  wrapper.find('withStyles(DateRangePicker)').prop('onDatesChange')({
    startDate: moment('2020-02-20'),
    endDate: moment('2020-02-25'),
  });

  expect(setStartDateSpy).toHaveBeenLastCalledWith(moment('2020-02-20'));
  expect(setEndDateSpy).toHaveBeenLastCalledWith(moment('2020-02-25'));
});

test('should handle date focus changes', () => {
  wrapper.find('withStyles(DateRangePicker)').prop('onFocusChange')(
    'startDate'
  );

  expect(wrapper.state('calendarFocused')).toBe('startDate');
});

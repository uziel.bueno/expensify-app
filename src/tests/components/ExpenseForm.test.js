import React from 'react';
import { shallow as shallowRender } from 'enzyme';
import moment from 'moment';
import ExpenseForm from '@components/ExpenseForm';
import expenses from '../fixtures/expenses';

test('should render ExpenseForm correctly', () => {
  const wrapper = shallowRender(<ExpenseForm />);
  expect(wrapper).toMatchSnapshot();
});

test('should render ExpenseForm with expense data', () => {
  const wrapper = shallowRender(<ExpenseForm expense={expenses[1]} />);
  expect(wrapper).toMatchSnapshot();
});

test('should render error for invalid form submission', () => {
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper.find('form').simulate('submit', { preventDefault: () => {} });

  expect(wrapper.state('error').length).toBeGreaterThan(0);
  expect(wrapper).toMatchSnapshot();
});

test('should set description on input change', () => {
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper
    .find('input[name="description"]')
    .simulate('change', { target: { value: 'Gum' } });

  expect(wrapper.state('description')).toBe('Gum');
  expect(wrapper).toMatchSnapshot();
});

test('should set note on textarea change', () => {
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper.find('textarea[name="note"]').simulate('change', {
    target: { value: 'Pay the bill tomorrow' },
    persist: () => {},
  });

  expect(wrapper.state('note')).toBe('Pay the bill tomorrow');
  expect(wrapper).toMatchSnapshot();
});

test('should set amount if input is valid', () => {
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper
    .find('input[name="amount"]')
    .simulate('change', { target: { value: '12.50' } });

  expect(wrapper.state('amount')).toBe('12.50');
  expect(wrapper).toMatchSnapshot();
});

test('should not set amount if input is invalid', () => {
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper
    .find('input[name="amount"]')
    .simulate('change', { target: { value: '123.123' } });

  expect(wrapper.state('amount')).toBe('');
  expect(wrapper).toMatchSnapshot();
});

test('should call onSubmit prop for valid form submission', () => {
  const onSubmitSpy = jest.fn();
  const wrapper = shallowRender(
    <ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy} />
  );

  wrapper.find('form').simulate('submit', { preventDefault: () => {} });

  expect(wrapper.state('error')).toBe('');
  expect(onSubmitSpy).toHaveBeenLastCalledWith({
    description: expenses[0].description,
    amount: expenses[0].amount,
    createdAt: expenses[0].createdAt,
    note: expenses[0].note,
  });
});

test('should set new date on date change', () => {
  const date = moment('2020-02-22');
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper.find('withStyles(SingleDatePicker)').prop('onDateChange')(date);

  expect(wrapper.state('createdAt')).toEqual(date);
});

test('should set calendar focus on change', () => {
  const focused = true;
  const wrapper = shallowRender(<ExpenseForm />);

  wrapper.find('withStyles(SingleDatePicker)').prop('onFocusChange')({
    focused,
  });

  expect(wrapper.state('calendarFocused')).toBe(focused);
});

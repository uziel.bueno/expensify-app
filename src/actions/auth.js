import { firebase, googleProvider } from '@/firebase/firebase';
export const login = (uid) => ({
  type: 'LOGIN',
  uid,
});

export const startLogin = () => {
  return (dispatch, getState) => {
    return firebase
      .auth()
      .signInWithPopup(googleProvider)
      .then((user) => {
        // dispatch(startLogin(user));
        return user;
      })
      .catch((error) => console.error(error));
  };
};

export const logout = () => ({
  type: 'LOGOUT',
});

export const startLogout = () => {
  return (dispatch) => {
    return firebase
      .auth()
      .signOut()
      .then(() => {})
      .catch((error) => console.error(error));
  };
};

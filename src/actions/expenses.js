import database from '@/firebase/firebase';

// ADD_EXPENSE
export const addExpense = (expense) => ({
  type: 'ADD_EXPENSE',
  expense,
});

export const startAddExpense = (expenseData = {}) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;

    const {
      description = '',
      amount = 0,
      createdAt = 0,
      note = '',
    } = expenseData;
    const expense = { description, amount, createdAt, note };

    return database
      .ref(`users/${uid}/expenses`)
      .push(expense)
      .then((ref) => {
        dispatch(addExpense({ id: ref.key, ...expense }));
      })
      .catch((error) => console.error(error));
  };
};

// REMOVE_EXPENSE
export const removeExpense = ({ id = '' }) => ({
  type: 'REMOVE_EXPENSE',
  id,
});

export const startRemoveExpense = ({ id = '' }) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;

    return database
      .ref(`users/${uid}/expenses/${id}`)
      .remove()
      .then((value) => {
        dispatch(removeExpense({ id }));
        return value;
      })
      .catch();
  };
};

// EDIT_EXPENSE
export const editExpense = (id, updates) => ({
  type: 'EDIT_EXPENSE',
  expense: { id, updates },
});

export const startEditExpense = (id, updates) => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;

    return database
      .ref(`users/${uid}/expenses/${id}`)
      .update(updates)
      .then((value) => {
        dispatch(editExpense(id, updates));
        return value;
      })
      .catch((err) => console.err(err));
  };
};

// SET_EXPENSES
export const setExpenses = (expenses) => ({
  type: 'SET_EXPENSES',
  expenses,
});

export const startSetExpenses = () => {
  return (dispatch, getState) => {
    const uid = getState().auth.uid;

    return database
      .ref(`users/${uid}/expenses`)
      .once('value')
      .then((snapshot) => {
        const expenses = [];

        snapshot.forEach((childSnapshot) => {
          expenses.push({ id: childSnapshot.key, ...childSnapshot.val() });
        });

        dispatch(setExpenses(expenses));
        return expenses;
      })
      .catch((error) => console.error(error));
  };
};

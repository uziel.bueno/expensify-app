import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import numeral from 'numeral';

const ExpenseListItem = ({ id, description, amount, createdAt }) => (
  <Link to={`/edit/${id}`} className="list-item">
    <div>
      <h3 className="list-item__title">{description}</h3>
      <span className="list-item__subtitle">
        {moment(createdAt).format('DD MMM, YYYY')}
      </span>
    </div>
    <h3 className="list-item__data">
      {numeral(amount)
        .divide(100)
        .format('$0,0.00')}
    </h3>
  </Link>
);

// If no data is required from the state there is no need to pass mapStateToProps
export default ExpenseListItem;

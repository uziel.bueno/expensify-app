import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { login, logout } from './actions/auth';
import { firebase } from '@/firebase/firebase';
import configureStore from './store/configureStore';
import LoadingPage from '@components/LoadingPage';
import { startSetExpenses } from './actions/expenses';
import AppRouter, { history } from './routers/AppRouter';
import 'react-dates/lib/css/_datepicker.css';
import './styles/styles.scss';

const store = configureStore();

store.subscribe(() => {
  // const state = store.getState();
  // console.table(state.expenses);
  // console.log(getVisibleExpenses(state.expenses, state.filters));
});

const jsx = (
  <Provider store={store}>
    <AppRouter />
  </Provider>
);

ReactDOM.render(<LoadingPage />, document.getElementById('app'));

let hasRendered = false;

const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(jsx, document.getElementById('app'));
    hasRendered = true;
  }
};

firebase.auth().onAuthStateChanged((user) => {
  if (user) {
    // Set the uid in the store to test if user is logged in...
    store.dispatch(login(user.uid));

    // Obtain the expenses stored in the database...
    store.dispatch(startSetExpenses()).then((expenses) => {
      renderApp();

      if (history.location.pathname === '/') {
        history.replace('/dashboard');
      }
    });

    return;
  }

  store.dispatch(logout());
  renderApp();
  history.replace('/');
});

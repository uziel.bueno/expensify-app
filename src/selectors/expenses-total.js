/**
 * Calculates the total amount of an array of expenses.
 *
 * @param {array} expenses
 */
export default function selectExpensesTotal(expenses = []) {
  return expenses
    .map((expense) => expense.amount)
    .reduce((sum, value) => sum + value, 0);
}

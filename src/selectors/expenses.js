import moment from 'moment';

const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) => {
  return expenses
    .filter((expense) => {
      const createdAtMoment = moment(expense.createdAt);
      // If startDate is not a number then it's a match (because there's no way of do the match),
      // otherwise try to match with the expression.
      const startDateMatch = startDate
        ? startDate.isSameOrBefore(createdAtMoment, 'day')
        : true;
      const endDateMatch = endDate
        ? endDate.isSameOrAfter(createdAtMoment, 'day')
        : true;
      const textMatch =
        typeof text !== 'string' ||
        expense.description.toLowerCase().includes(text.toLowerCase());

      return startDateMatch && endDateMatch && textMatch;
    })
    .sort((currentExpense, nextExpense) => {
      if (sortBy === 'date') {
        // Most recent expenses first...
        return currentExpense.createdAt > nextExpense.createdAt ? -1 : 1;
      }

      if (sortBy === 'amount') {
        // Most expensive expenses first...
        return currentExpense.amount > nextExpense.amount ? -1 : 1;
      }

      return 0;
    });
};

export default getVisibleExpenses;

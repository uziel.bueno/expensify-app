const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

if (process.env.NODE_ENV === 'test') {
  require('dotenv').config({ path: '.env.test' });
}
if (process.env.NODE_ENV === 'development') {
  require('dotenv').config({ path: '.env.development' });
}

module.exports = (env, argv) => ({
  mode: env.production ? 'production' : 'development',
  // entry: './src/playground/hoc.js',
  entry: ['babel-polyfill', './src/app.js'],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        loader: 'babel-loader',
        test: /\.js$/,
        exclude: /node_modules/,
      },
      {
        test: /\.(sc|c)ss$/,
        use: [
          env.production
            ? {
                loader: MiniCssExtractPlugin.loader,
                // options: { publicPath: path.join(__dirname, 'public') },
              }
            : 'style-loader',
          {
            loader: 'css-loader',
            options: { sourceMap: true },
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true,
              // sassOptions: {
              //   includePaths: [
              //     path.join(__dirname, 'node_modules'),
              //     path.join(__dirname, 'src/styles'),
              //   ],
              // },
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new webpack.EnvironmentPlugin([
      'FIREBASE_API_KEY',
      'FIREBASE_AUTH_DOMAIN',
      'FIREBASE_DATABASE_URL',
      'FIREBASE_PROJECT_ID',
      'FIREBASE_STORAGE_BUCKET',
      'FIREBASE_MESSAGING_SENDER_ID',
      'FIREBASE_APP_ID',
    ]),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: env.production ? 'css/´styles.[hash].css' : 'css/styles.css',
      chunkFilename: env.production ? 'css/[id].[hash].css' : 'css/[id].css',
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: './public/index.html',
      favicon: path.resolve(__dirname, 'public/img/favicon.png'),
      minify: false,
    }),
  ],
  devtool: env.production ? 'source-map' : 'inline-source-map',
  resolve: {
    alias: {
      '@': path.join(__dirname, 'src'),
      '@components': path.join(__dirname, 'src/components'),
    },
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
    historyApiFallback: true,
    overlay: {
      warnings: true,
      errors: true,
    },
  },
});
